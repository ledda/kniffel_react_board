import KniffelCell, { createCellFromDef, KniffelCellValue, CellState } from "./KniffelCell";
import {CellDef, BlockDef, BonusBlockDef, NoBonusBlockDef } from "../static/rulesets";

export const createBlockFromDef = (blockDef: BlockDef) : ScoreBlock => {
    if (blockDef.hasBonus) {
        return new ScoreBlockWithBonus(blockDef);
    }
    else {
        return new ScoreBlockNoBonus(blockDef);
    }
};

export interface BlockState {
    id: string;
    cellStates: CellState[];
}

abstract class ScoreBlock {
    protected cells: KniffelCell[];
    protected id: string;

    protected constructor(blockDef: BlockDef) {
        this.cells = ScoreBlock.generateCells(blockDef.cells);
        this.id = blockDef.id;
    }

    private static generateCells(cellDefs: CellDef[]): KniffelCell[] {
        const cells = [];
        for (const cellDef of cellDefs) {
            cells.push(createCellFromDef(cellDef));
        }
        return cells;
    }

    abstract getTotal(): number;

    abstract bonusAttained(): boolean;

    getSubtotal(): number {
        let blockScore = 0;
        for (const cell of this.cells) {
            blockScore += cell.getScore();
        }
        return blockScore;
    }

    updateCellByIdWithValue(cellId: string, value: KniffelCellValue): void {
        this.getCellById(cellId).update(value);
    }

    getCellScoreById(cellId: string): KniffelCellValue {
        return this.getCellById(cellId).getScore();
    }

    cellWithIdIsStruck(cellId: string): boolean {
        return this.getCellById(cellId).isStruck();
    }

    private getCellById(cellId: string): KniffelCell {
        const foundKniffelCell = this.cells.find(cell => cell.getId() === cellId);
        if (foundKniffelCell !== undefined) {
            return foundKniffelCell;
        }
        else {
            throw new Error("KniffelCell with ID " + cellId + " not found in block with ID " + this.id + "!")
        }
    }

    getId(): string {
        return this.id;
    }

    getState(): BlockState {
        const state: BlockState = {
            id: this.id,
            cellStates: []
        };
        state.cellStates = this.cells.map(cell => cell.getState());
        return Object.assign({}, state);
    }

    restoreCellsFromStates(cellStates: CellState[]): void {
        cellStates.forEach(cellState => {
            const correspondingCell = this.getCellById(cellState.id);
            correspondingCell.restoreFromState(cellState);
        })
    }
}

class ScoreBlockWithBonus extends ScoreBlock {
    protected readonly bonus: number;
    protected readonly bonusFor: number;

    constructor(blockDef: BonusBlockDef) {
        super(blockDef);
        this.bonus = blockDef.bonusScore;
        this.bonusFor = blockDef.bonusFor;
    }

    getTotal(): number {
        const prelimScore = this.getSubtotal();
        return prelimScore >= this.bonusFor ? prelimScore + this.bonus : prelimScore;
    }

    bonusAttained(): boolean {
        return this.getSubtotal() >= this.bonusFor;
    }
}

class ScoreBlockNoBonus extends ScoreBlock {
    constructor(blockDef: NoBonusBlockDef) {
        super(blockDef);
    }

    getTotal(): number {
        return this.getSubtotal();
    }

    bonusAttained(): boolean {
        return false;
    }
}

export default ScoreBlock;