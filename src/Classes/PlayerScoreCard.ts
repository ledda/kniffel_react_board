import {KniffelCellValue} from "./KniffelCell";
import ScoreBlock, { createBlockFromDef, BlockState } from "./ScoreBlock";
import {BlockDef, GameSchema} from "../static/rulesets";
import { Memento, Originator } from "./Caretaker";

export type CellLocation = { blockId: string, cellId: string };

class PlayerScoreCard implements Originator {
    private readonly playerId: string;
    private readonly blocks: ScoreBlock[];

    constructor(playerId: string, gameSchema: GameSchema) {
        this.playerId = playerId;
        this.blocks = PlayerScoreCard.generateBlocks(gameSchema.blocks);
    }

    private static generateBlocks(blockDefs: BlockDef[]): ScoreBlock[] {
        const blocks = [];
        for (const blockDef of blockDefs) {
            blocks.push(createBlockFromDef(blockDef));
        }
        return blocks;
    }

    getTotal(): number {
        let playerTotal = 0;
        for (const block of this.blocks) {
            playerTotal += block.getTotal();
        }
        return playerTotal;
    }

    getBlockTotalById(blockId: string): number {
        return this.getBlockById(blockId).getTotal();
    }

    getBlockSubTotalById(blockId: string): number {
        return this.getBlockById(blockId).getSubtotal();
    }

    blockWithIdHasBonus(blockId: string): boolean {
        return this.getBlockById(blockId).bonusAttained();
    }

    updateCellByLocationWithValue(loc: CellLocation, value: KniffelCellValue): void {
        this.getBlockById(loc.blockId).updateCellByIdWithValue(loc.cellId, value);
    }

    getCellScoreByLocation(loc: CellLocation): KniffelCellValue {
        return this.getBlockById(loc.blockId).getCellScoreById(loc.cellId);
    }

    cellAtLocationIsStruck(loc: CellLocation): boolean {
        return this.getBlockById(loc.blockId).cellWithIdIsStruck(loc.cellId);
    }

    getSnapshot(): Memento {
        return new PlayerScoreCardMemento(this.getState());
    }

    private getState(): PlayerScoreCardState {
        const state: PlayerScoreCardState = {
            blocks: []
        };
        state.blocks = this.blocks.map(block => block.getState());
        return state;
    }

    restoreSnapshot(snapshot: PlayerScoreCardMemento): void {
        const state = snapshot.getState();
        state.blocks.forEach(block => {
            const correspondingBlock = this.getBlockById(block.id);
            correspondingBlock.restoreCellsFromStates(block.cellStates);
        });
    }

    private getBlockById(blockId: string): ScoreBlock {
        const foundScoreBlock = this.blocks.find(block => block.getId() === blockId);
        if (foundScoreBlock !== undefined) {
            return foundScoreBlock;
        }
        else {
            throw new Error("ScoreBlock with ID " + blockId + " not found for player" + this.playerId + "!")
        }
    }
}

interface PlayerScoreCardState {
    blocks: BlockState[];
}

class PlayerScoreCardMemento implements Memento {
    private readonly state: PlayerScoreCardState;

    constructor(state: PlayerScoreCardState) {
        this.state = state;
    }

    getState(): PlayerScoreCardState {
        return this.state;
    }
}

export default PlayerScoreCard;