import React, {ChangeEvent, FocusEvent, ReactNode, KeyboardEvent} from "react";
import {CellFlag, FieldType} from "../static/enums";
import {KniffelCellValue} from "../Classes/KniffelCell";
import {CellEventResponse} from "./KniffelBoard";
import {CellLocation} from "../Classes/PlayerScoreCard";
import {useLongPress} from "./useLongPress";

export interface KniffelCellRendererProps {
    location: CellLocation;
    fieldType: FieldType;
    playerId: string;
    value: KniffelCellValue | CellFlag;
    showResults?: boolean;
    onCellEdit: (response: CellEventResponse) => void;
}

interface KniffelCellRendererState {}

class KniffelCellRenderer extends React.Component<KniffelCellRendererProps, KniffelCellRendererState> {
    private standardTimeoutTimeMs: number;
    constructor(props: KniffelCellRendererProps) {
        super(props);
        this.standardTimeoutTimeMs = 400;
    }

    updateCell = (value: KniffelCellValue): void => {
        const response: CellEventResponse = {
            value: value,
            playerId: this.props.playerId,
            location: this.props.location,
        };
        this.props.onCellEdit(response);
    };

    strikeCell = (): void => {
        const response: CellEventResponse = {
            value: CellFlag.strike,
            playerId: this.props.playerId,
            location: this.props.location,
        };
        this.props.onCellEdit(response);
    };

    unstrikeCell = (): void => {
        const response: CellEventResponse = {
            value: CellFlag.unstrike,
            playerId: this.props.playerId,
            location: this.props.location,
        };
        this.props.onCellEdit(response);
    };

    render(): ReactNode {
        const {
            fieldType,
            value,
            showResults,
        } = this.props;

        const propsForEditableCell = {
            timeoutMs: this.standardTimeoutTimeMs,
            updateCell: this.updateCell,
            strikeCell: this.strikeCell,
            value: value,
        };

        if (value === CellFlag.strike) {
            return <StrikeKniffelCell unstrikeCell={this.unstrikeCell} />;
        }
        else {
            switch (fieldType) {
                case FieldType.bonus:
                case FieldType.subtotal:
                case FieldType.total:
                case FieldType.globalTotal:
                    return (
                        <GenericResultsKniffelCell
                            classNameString={fieldType}
                            showResults={showResults as boolean}
                            value={value}
                        />
                    );
                case FieldType.bool:
                    return <BoolKniffelCell {...propsForEditableCell}/>;
                case FieldType.multiplier:
                    return <MultipleKniffelCell {...propsForEditableCell}/>;
                case FieldType.number:
                    return <NumberKniffelCell {...propsForEditableCell}/>;
                case FieldType.yahtzee:
                    return <YahtzeeKniffelCell {...propsForEditableCell}/>;
            }
        }
    }
}

interface StandardKniffelCellProps {
    value: KniffelCellValue,
}

interface StrikeKniffelCellProps {
    unstrikeCell: () => void,
}

interface ResultsKniffelCellProps extends StandardKniffelCellProps {
    showResults: boolean,
}

interface UpdateableKniffelCellProps extends StandardKniffelCellProps {
    updateCell: (updateVal: KniffelCellValue) => void,
}

interface LongPressStrikeKniffelCellProps extends StandardKniffelCellProps {
    timeoutMs: number,
    strikeCell: () => void,
}

interface GenericResultsKniffelCellProps extends ResultsKniffelCellProps {
    classNameString: string;
}

type EditableKniffelCellProps = UpdateableKniffelCellProps & LongPressStrikeKniffelCellProps;

const NumberKniffelCell: React.FunctionComponent<EditableKniffelCellProps> = ({ strikeCell, updateCell, value , timeoutMs}) => {
    const [beingEdited, updateBeingEdited] = React.useState(false);
    const [currentEditValue, updateCurrentEditValue] = React.useState("");
    const strikeCellOnLongPress = useLongPress(strikeCell, timeoutMs);

    const displayText: string = beingEdited ? currentEditValue : value.toString();

    const handleFocus = (e: FocusEvent<HTMLInputElement>) => {
        updateBeingEdited(true);
    };

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        updateCurrentEditValue(e.target.value);
        console.log(e.target.value);
        if (e.target.value == "") {
            strikeCell();
            updateBeingEdited(false);
            updateCurrentEditValue("");
        }
    };

    const handleBlur = (e: FocusEvent<HTMLInputElement>) => {
        updateCell(Number(e.target.value));
        updateBeingEdited(false);
        updateCurrentEditValue("");
    };

    const handleKeyUp = (e: KeyboardEvent<HTMLInputElement>) => {
        if (e.key === "Enter") {
            updateCell(Number(e.currentTarget.value));
            updateBeingEdited(false);
            updateCurrentEditValue("");
        }
    };

    return (
        <td className={"kniffelCell numberField"}>
            <div className={"numberField"}>
                <input
                    type={"number"}
                    onFocus={handleFocus}
                    onBlur={handleBlur}
                    onInput={handleChange}
                    onChange={handleChange}
                    onKeyUp={handleKeyUp}
                    value={displayText}
                    className={"numberField"}
                    onAuxClick={strikeCell}
                    {...strikeCellOnLongPress}
                />
            </div>
        </td>
    )
};

const YahtzeeKniffelCell: React.FunctionComponent<EditableKniffelCellProps> = ({value, timeoutMs, strikeCell, updateCell}) => {
    const handleClick = (): void => updateCell(true);
    const strikeCellOnLongPress = useLongPress(strikeCell, timeoutMs);
    return (
        <td className={"kniffelCell yahtzeeField"}>
            <div
                className={"yahtzeeField"}
                onClick={handleClick}
                {...strikeCellOnLongPress}
            >
                {value}
            </div>
        </td>
    )
};

const BoolKniffelCell: React.FunctionComponent<EditableKniffelCellProps> = ({value, timeoutMs, strikeCell, updateCell}) => {
    const handleClick = (): void => updateCell(true);
    const strikeCellOnLongPress = useLongPress(strikeCell, timeoutMs);
    return (
        <td
            className={"kniffelCell boolField" + (value ? "_checked" : "_unchecked")}
            onClick={handleClick}
            {...strikeCellOnLongPress}
        >
            <div className="checkmark" />
        </td>
    )
};

const MultipleKniffelCell: React.FunctionComponent<EditableKniffelCellProps> = ({value, timeoutMs, strikeCell, updateCell}) => {
    const handleClick = (): void => updateCell(true);
    const strikeCellOnLongPress = useLongPress(strikeCell, timeoutMs);
    return (
        <td className={"kniffelCell multipleField"}>
            <div
                className={"multipleField"}
                onClick={handleClick}
                {...strikeCellOnLongPress}
            >
                {value}
            </div>
        </td>
    )
};

const GenericResultsKniffelCell: React.FunctionComponent<GenericResultsKniffelCellProps> = ({showResults, value, classNameString}) => {
    return (
        <td className={"kniffelCell " + classNameString}>
            <div className={classNameString + (showResults ? "" : " hideResults")}>
                {value}
            </div>
        </td>
    )
};

const StrikeKniffelCell: React.FunctionComponent<StrikeKniffelCellProps> = ({unstrikeCell}) => {
    const updateCell = () => unstrikeCell();
    return (
        <td
            className={"kniffelCell strikeCell"}
            onClick={updateCell}
        >
            <span className="strikeText">X</span>
        </td>
    );
};

export default KniffelCellRenderer;