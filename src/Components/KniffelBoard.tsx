import React, {ReactNode, useContext} from "react";
import PlayerScoreCard, {CellLocation} from "../Classes/PlayerScoreCard";
import {BlockDef, GameSchema, getGameSchemaById} from "../static/rulesets";
import {formatUnicorn, LocaleContext} from "../static/strings";
import {CellFlag, FieldType} from "../static/enums";
import {KniffelCellValue} from "../Classes/KniffelCell";
import {CaretakerSet} from "../Classes/Caretaker";
import KniffelCellRenderer from "./KniffelCellRenderer";
import {GameSettings} from "./GameSetup";

const maxHistoryLength = 256;

type CellDisplayValue = KniffelCellValue | CellFlag.strike;

export interface CellEventResponse {
    value: KniffelCellValue | CellFlag;
    playerId: string;
    location: CellLocation;
}

class KniffelBoard extends React.Component<KniffelBoardProps, KniffelBoardState> {
    private readonly gameSchema: GameSchema;
    private readonly caretaker: CaretakerSet;
    state: KniffelBoardState;

    constructor(props: KniffelBoardProps) {
        super(props);

        this.gameSchema = getGameSchemaById(this.props.settings.ruleset);

        this.state = {
            scoreSheet: this.generateNewScoreSheet(this.props.settings.playerIds),
            playerIds: this.props.settings.playerIds,
            showResults: true
        };

        this.caretaker = new CaretakerSet(
            maxHistoryLength,
            ...this.state.playerIds.map(
            pid => this.state.scoreSheet[pid]
            )
        );
    }

    private generateNewScoreSheet(playerIds: string[]): ScoreSheet {
        const scoreSheet: ScoreSheet = {};
        for (const playerId of playerIds) {
            scoreSheet[playerId] = new PlayerScoreCard(playerId, this.gameSchema);
        }
        return scoreSheet;
    }

    private onCellEdit = (response: CellEventResponse): void => {
        const newScoreSheet = this.state.scoreSheet;
        KniffelBoard.updateScoreSheetFromCellResponse(newScoreSheet, response);
        this.setState({ scoreSheet: newScoreSheet });
        this.caretaker.save();
    };

    private static updateScoreSheetFromCellResponse(scoreSheet: ScoreSheet, response: CellEventResponse): void {
        const playerScoreCard = scoreSheet[response.playerId];
        playerScoreCard.updateCellByLocationWithValue(response.location, response.value);
    }

    toggleShowResults = () => {
        this.setState({ showResults: !this.state.showResults });
    };

    private getCellDisplayValueByPlayerIdAndLocation(playerId: string, location: CellLocation): CellDisplayValue {
        const playerSheet = this.state.scoreSheet[playerId];
        let cellValue = playerSheet.getCellScoreByLocation(location);
        cellValue = playerSheet.cellAtLocationIsStruck(location) ? CellFlag.strike : cellValue;
        return cellValue;
    };

    private getBlockSubtotalByPlayerId(blockId: string, playerId: string): number {
        return this.state.scoreSheet[playerId].getBlockSubTotalById(blockId);
    }

    private getBlockTotalByPlayerId(blockId: string, playerId: string): number {
        return this.state.scoreSheet[playerId].getBlockTotalById(blockId);
    }

    private getTotalForPlayer(playerId: string): number {
        return this.state.scoreSheet[playerId].getTotal();
    }

    private playerHasBonusForBlock(playerId: string, blockId: string): boolean {
        return this.state.scoreSheet[playerId].blockWithIdHasBonus(blockId);
    }

    private undo(): void {
        this.caretaker.undo();
        this.forceUpdate();
    }

    private redo(): void {
        this.caretaker.redo();
        this.forceUpdate();
    }

    BonusRowForBlock: React.FunctionComponent<KniffelBonusRowProps> = ({ blockId, bonusScore, showResults }) => {
        const cells: ReactNode[] = [];
        for (const playerId of this.state.playerIds) {
            cells.push((
                <KniffelCellRenderer
                    key={"cell_bonus_" + blockId + "_" + playerId}
                    location={{ blockId, cellId: "bonus"}}
                    fieldType={FieldType.bonus}
                    playerId={playerId}
                    value={this.playerHasBonusForBlock(playerId, blockId) ? bonusScore : 0}
                    onCellEdit={() => {}}
                    showResults={showResults}
                />
            ));
        }
        return <>{cells}</>;
    };

    SubtotalRowForBlock: React.FunctionComponent<KniffelTotalRowProps> = ({ blockId, showResults }) => {
        const cells: ReactNode[] = [];
        for (const playerId of this.state.playerIds) {
            cells.push((
                <KniffelCellRenderer
                    key={"cell_subtotal_" + blockId + "_" + playerId}
                    location={{ blockId, cellId: "subtotal"}}
                    fieldType={FieldType.subtotal}
                    playerId={playerId}
                    value={this.getBlockSubtotalByPlayerId(blockId, playerId)}
                    onCellEdit={() => {}}
                    showResults={showResults}
                />
            ));
        }
        return <>{cells}</>;
    };

    TotalRowForBlock: React.FunctionComponent<KniffelTotalRowProps> = ({ blockId, showResults }) => {
        const cells: ReactNode[] = [];
        for (const playerId of this.state.playerIds) {
            cells.push((
                <KniffelCellRenderer
                    key={"cell_total_" + blockId + "_" + playerId}
                    location={{ blockId, cellId: "total"}}
                    fieldType={FieldType.subtotal}
                    playerId={playerId}
                    value={this.getBlockTotalByPlayerId(blockId, playerId)}
                    onCellEdit={() => {}}
                    showResults={showResults}
                />
            ));
        }
        return <>{cells}</>;
    };

    GrandTotalRow: React.FunctionComponent<KniffelGrandTotalRowProps> = ({ showResults }) => {
        const cells: ReactNode[] = [];
        for (const playerId of this.state.playerIds) {
            cells.push((
                <KniffelCellRenderer
                    key={"cell_grandtotal_" + playerId}
                    location={{blockId: "global", cellId: "grandTotal"}}
                    fieldType={FieldType.subtotal}
                    playerId={playerId}
                    value={this.getTotalForPlayer(playerId)}
                    onCellEdit={() => {}}
                    showResults={showResults}
                />
            ));
        }
        return <>{cells}</>;
    };

    render(): ReactNode {
        const Locale = this.context.strings;

        const EditableCellsForRow: React.FunctionComponent<EditableCellsForRowProps> = ({ location, fieldType }) => {
            const cells: ReactNode[] = [];

            for (const playerId of this.state.playerIds) {
                cells.push((
                    <KniffelCellRenderer
                        key={"cell_" + location.cellId + "_" + playerId}
                        location={location}
                        fieldType={fieldType}
                        playerId={playerId}
                        value={this.getCellDisplayValueByPlayerIdAndLocation(playerId, location)}
                        onCellEdit={this.onCellEdit}
                    />
                ));
            }

            return <>{cells}</>;
        };

        const BlockRenderer: React.FunctionComponent<BlockRendererProps> = ({ blockSchema }) => {
            const rowsInBlock: ReactNode[] = [];

            for (const cell of blockSchema.cells) {
                rowsInBlock.push((
                    <RowContainer
                        key={"rowCont" + cell.id + blockSchema.id}
                        label={cell.label}
                        cellCssClassName={cell.fieldType}
                    >
                        <EditableCellsForRow
                            location={{blockId: blockSchema.id, cellId: cell.id}}
                            fieldType={cell.fieldType}
                        />
                    </RowContainer>
                ));
            }
            if (blockSchema.hasBonus) {
                rowsInBlock.push(
                    <RowContainer
                        key={"rowContSubtotal" + blockSchema.id}
                        label={Locale.rowLabels.subtotal}
                        cellCssClassName={FieldType.subtotal}
                    >
                        {this.SubtotalRowForBlock({blockId: blockSchema.id, showResults: this.state.showResults})}
                    </RowContainer>
                );
                rowsInBlock.push(
                    <RowContainer
                        key={"rowContBonus" + blockSchema.id}
                        label={Locale.rowLabels.bonus}
                        cellCssClassName={FieldType.bonus}
                    >
                        {this.BonusRowForBlock({
                            blockId: blockSchema.id,
                            bonusScore: blockSchema.bonusScore,
                            showResults: this.state.showResults
                        })}
                    </RowContainer>
                );
            }
            rowsInBlock.push(
                <RowContainer
                    key={"rowCont"}
                    label={formatUnicorn(Locale.rowLabels.blockTotal, blockSchema.label)}
                    cellCssClassName={FieldType.total}
                >
                    {this.TotalRowForBlock({ blockId: blockSchema.id, showResults: this.state.showResults })}
                </RowContainer>
            );

            return <>{rowsInBlock}</>;
        };

        const RowContainer: React.FunctionComponent<RowContainerProps> = ({ cellCssClassName, label, children }) => {
            return (
                <tr className={"kniffelRow " + cellCssClassName}>
                    <td className="kniffelCell rowLabelCell">{label}</td>
                    {children}
                </tr>
            );
        };

        const rows: ReactNode[] = [];
        for (const block of this.gameSchema.blocks) {
            rows.push(<BlockRenderer key={"block" + block.id} blockSchema={block} />)
        }
        rows.push(
            <RowContainer
                key={"rowContGrandTotal"}
                label={Locale.rowLabels.globalTotal}
                cellCssClassName={FieldType.total}
            >
                {this.GrandTotalRow({ showResults: this.state.showResults })}
            </RowContainer>
        );

        return (
            <div className="game">
                <table className="kniffelTable">
                    <thead>
                        <tr>
                            <th colSpan={this.state.playerIds.length + 1}>
                                {Locale.boardTitle}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <ColumnHeadersRow playerIds={this.state.playerIds} />
                        {rows}
                    </tbody>
                </table>
                <div className="buttonContainer">
                    <button type="button" className="showResultsButton" onClick={this.toggleShowResults}>
                        {this.state.showResults ?
                            Locale.buttons.showHideResultsButton.hide :
                            Locale.buttons.showHideResultsButton.show}
                    </button>
                    <button
                        type="button"
                        className="undoButton"
                        disabled={!this.caretaker.undosLeft()}
                        onClick={() => this.undo()}
                    >
                        {Locale.buttons.undoButton}
                    </button>
                    <button
                        type="button"
                        className="redoButton"
                        disabled={!this.caretaker.redosLeft()}
                        onClick={() => this.redo()}
                    >
                        {Locale.buttons.redoButton}
                    </button>
                </div>
                <div className="buttonContainer">
                    <button
                        type="button"
                        className="backButton"
                        onClick={() => this.props.returnToSetup()}
                    >
                        {Locale.buttons.returnToSetupButton}
                    </button>
                </div>
            </div>
        );
    }
}
KniffelBoard.contextType = LocaleContext;

export interface KniffelBoardProps {
    settings: GameSettings;
    returnToSetup: () => void;
}

interface KniffelBoardState {
    scoreSheet: ScoreSheet;
    playerIds: string[];
    showResults: boolean;
}

interface ScoreSheet {
    [key: string]: PlayerScoreCard;
}

interface RowContainerProps {
    cellCssClassName: string;
    label: string;
}

interface KniffelBonusRowProps {
    blockId: string;
    bonusScore: number;
    showResults: boolean;
}

interface KniffelTotalRowProps {
    blockId: string;
    showResults: boolean;
}

interface KniffelGrandTotalRowProps {
    showResults: boolean;
}

interface EditableCellsForRowProps {
    location: CellLocation;
    fieldType: FieldType;
}

interface BlockRendererProps {
    blockSchema: BlockDef;
}

const ColumnHeadersRow: React.FunctionComponent<ColumnHeadersRowProps> = ({ playerIds }) => {
    const locale = useContext(LocaleContext).strings;

    const columnHeaders: ReactNode[] = [(
        <td className="topLeftBlankCell" key={"blank_header"}>
            {locale.headers.rowLabels}
        </td>
    )];
    for (const playerId of playerIds) {
        columnHeaders.push(
            <td className="playerNameCell" key={"header" + playerId}>
                {playerId}
            </td>
        );
    }
    return (
        <tr className="columnHeaderRow">
            {columnHeaders}
        </tr>
    );
};

interface ColumnHeadersRowProps {
    playerIds: string[];
}

export default KniffelBoard;
