import React, {ReactNode} from "react";
import "./App.css";
import KniffelBoard from "./Components/KniffelBoard";
import GameSetup, {GameSettings} from "./Components/GameSetup"
import Settings from "./static/settings.json";
import {SupportedLang} from "./static/enums";
import {LocaleContext, localeDefaultVal} from "./static/strings";

class App extends React.Component<AppProps, AppState> {
    state: AppState;

    constructor(props: AppProps) {
        super(props);

        const startupSettings: GameSettings = {
            playerIds: Settings.players,
            ruleset: Settings.ruleset,
            lang: Settings.lang as SupportedLang
        };

        this.state = {
            currentSettings: startupSettings,
            settingUp: true,
        };
    }

    onSetupComplete: (gameSettings: GameSettings) => void = (gameSettings) => {
        this.setState({
            currentSettings: gameSettings,
            settingUp: false
        });
    };

    returnToSetup: () => void = () => {
        this.setState({settingUp: true});
    };

    render(): ReactNode {
        return (
            <LocaleContext.Provider value={localeDefaultVal}>
                {this.state.settingUp ?
                    (
                        <GameSetup
                            onSetupComplete={this.onSetupComplete}
                            settings={this.state.currentSettings}
                        />
                    ) :
                    (
                        <KniffelBoard
                            settings={this.state.currentSettings}
                            returnToSetup={this.returnToSetup}
                        />
                    )
                }
            </LocaleContext.Provider>
        );
    }
}

interface AppState {
    currentSettings: GameSettings;
    settingUp: boolean;
}

interface AppProps {}

export default App;